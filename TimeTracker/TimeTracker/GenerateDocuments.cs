﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Diagnostics;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using Microsoft.Office.Interop;
using TimeTracker.Objects;
using TimeTracker.ViewModels;
using Excel = Microsoft.Office.Interop.Excel;

namespace TimeTracker
{
    static class GenerateDocuments
    {
        public static bool BuildExcelFiles(string userName, DateTime dt, ObservableCollection<object> shownList, ObservableCollection<object> defaultList)
        {
            //Generate grouping for Admin document
            var keyValue = new List<KeyValuePair<string, string>>();
            var groupList = new List<string>();
            foreach (ProjectRecordViewModel pr in shownList)
            {
                if (HoursHelper.HoursEntered(pr) > 0)
                {
                    string name = pr.ProjectName;

                    if (defaultList.Contains(name))
                    {
                        keyValue.Add(new KeyValuePair<string, string>("Admin", name));
                        if (!groupList.Contains("Admin"))
                        {
                            groupList.Add("Admin");
                        }
                    }
                    else
                    {
                        keyValue.Add(new KeyValuePair<string, string>(name, name));
                        groupList.Add(name);
                    }
                }
            }

            // Iterate through each group to create a seperate document for each
            foreach (string str in groupList)
            {
                int i = 0;

                // Get the values(ProjectNames) from the keyvalue pair
                var values = (from kvp in keyValue where kvp.Key == str select kvp.Value).ToList();

                const string templatepath = @"Templates\template.xlsx";
                var app = new Excel.Application { DisplayAlerts = false };
                //app.Visible = true;
                string cwd = Directory.GetCurrentDirectory();
                string path = Path.Combine(cwd, templatepath);
                var workbook = app.Workbooks.Open(path);
                var excelSheets = workbook.Worksheets;
                var excelWorksheet = (Excel.Worksheet)excelSheets.Item["Signoff Sheet"];
                // Might need to change back to this way?
                //var excelWorksheet = (Excel.Worksheet)excelSheets.get_Item("Signoff Sheet");


                foreach (ProjectRecordViewModel pr in shownList)
                {
                    // I need to check if the projectrecord group name is the same as str from the groupList we are currently running
                    string name = pr.ProjectName;
                    if (!values.Contains(name)) continue;

                    double totalHours = HoursHelper.HoursEntered(pr);
                    if (totalHours <= 0) continue;

                    excelWorksheet.Cells[1, 4] = userName;
                    excelWorksheet.Cells[2, 4] = (str == "Admin" ? "McsPro" : str);
                    excelWorksheet.Cells[1, 12] = dt.ToString("d");
                    excelWorksheet.Cells[48 + i, 1] = name;
                    excelWorksheet.Cells[6 + i, 1] = name;
                    excelWorksheet.Cells[6 + i, 5] = pr.Sun;
                    excelWorksheet.Cells[6 + i, 6] = pr.Mon;
                    excelWorksheet.Cells[6 + i, 7] = pr.Tue;
                    excelWorksheet.Cells[6 + i, 8] = pr.Wed;
                    excelWorksheet.Cells[6 + i, 9] = pr.Thu;
                    excelWorksheet.Cells[6 + i, 10] = pr.Fri;
                    excelWorksheet.Cells[6 + i, 11] = pr.Sat;

                    if (defaultList.Contains(name) || name == "Admin")
                        i++;
                }

                excelWorksheet.SaveAs(GetInitials(userName) + dt.AddDays(6).ToString("yyyyMMdd") + str + ".xlsx");
                app.Quit();
            }

            MessageBox.Show("Process complete, files saved in your Documents folder, press ok to close.");
            Process.Start(Environment.GetFolderPath(Environment.SpecialFolder.MyDocuments));

            // Add logic to determine if run was successful
            return true;
        }

        public static string GetInitials(string name)
        {
            string retval = name.Substring(0, 1);
            int lastspace = name.LastIndexOf(" ", StringComparison.Ordinal);
            if (lastspace < 0)
                lastspace++;
            retval += name.Substring(lastspace + 1, 1);
            retval = retval.ToUpper();

            return retval;
        }
    }
}
