﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Data.SQLite;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using TimeTracker.ViewModels;

namespace TimeTracker
{
    class DatabaseHelper
    {
        public static void SetupDB()
        {
            var list = MainWindowViewModel.This.CompleteProjectCollection;
            var defaultList = MainWindowViewModel.This.DefaultListCollection;
            
            SQLiteConnection conn = new SQLiteConnection("Data Source=TimeTracker.db");

            conn.Open();

            var command = conn.CreateCommand();

            //table Create if it doesn't already exist
            command.CommandText = "CREATE TABLE IF NOT EXISTS Project(id INTEGER PRIMARY KEY, Name Varchar(50), CanDelete INTEGER DEFAULT 1)";
            command.ExecuteNonQuery();

            

            //Count number of records in table
            command.CommandText = "SELECT count(*) FROM Project";
            int rowCount = 0;
            rowCount = Convert.ToInt32(command.ExecuteScalar());

            //If table doesn't contain any data add default values
            if (rowCount == 0)
            {
                foreach (string name in defaultList)
                {
                    // Each default value cannot be deleted.
                    int canDelete = 0;
                    //Inserting data
                    command.CommandText = "INSERT INTO Project(Name, CanDelete) values ('" + name + "', " + canDelete + ")";
                    command.ExecuteNonQuery();
                }
            }


            //Get Project list from Table
            command.CommandText = "SELECT * FROM Project ORDER BY id";
            SQLiteDataReader sdr = command.ExecuteReader();

            while (sdr.Read())
            {
                list.Add(sdr.GetString(1));
            }
            sdr.Close();

            /*
            //table Create
            command.CommandText = "CREATE TABLE IF NOT EXISTS std_tbl(id int, Name Varchar(30))";
            command.ExecuteNonQuery();

            //Inserting data
            command.CommandText = "INSERT INTO std_tbl(id, Name ) values (1, 'janak')";
            command.ExecuteNonQuery();

            //table Update
            command.CommandText = "UPDATE std_tbl SET Name='Zasmik' WHERE id = 1";
            command.ExecuteNonQuery();

            //Read from table
            command.CommandText = "SELECT * FROM std_tbl";
            SQLiteDataReader sdr = command.ExecuteReader();

            while (sdr.Read())
            {
                Debug.WriteLine(sdr.GetString(1));
            }
            sdr.Close();

            //Delete Record
            command.CommandText = "DELETE FROM std_tbl WHERE id=1";
            command.ExecuteNonQuery();
            */

            conn.Close();
        }

        public static void InsertRecord(string projname)
        {
            var list = MainWindowViewModel.This.CompleteProjectCollection;
            int recordcount = GetRecordProperty(projname, "count(*)");
            if (recordcount <= 0 && projname != string.Empty)
            {
                list.Insert(list.Count - 1, projname);

                SQLiteConnection conn = new SQLiteConnection("Data Source=TimeTracker.db");

                conn.Open();

                var command = conn.CreateCommand();

                command.CommandText = "INSERT INTO Project(Name) values ('" + projname + "')";
                command.ExecuteNonQuery();

                conn.Close();
            }
        }

        public static bool DeleteRecord(string projname)
        {
            var list = MainWindowViewModel.This.CompleteProjectCollection;
            var defaultList = MainWindowViewModel.This.DefaultListCollection;

            if (defaultList.Contains(projname))
            {
                //MessageBox.Show("You cannot delete a default value");
                return false;
            }

            if (MessageBox.Show("Are you sure you want to delete this record?", "Are you sure?", MessageBoxButton.OKCancel) == MessageBoxResult.OK)
            {
                list.Remove(projname);

                SQLiteConnection conn = new SQLiteConnection("Data Source=TimeTracker.db");

                conn.Open();

                var command = conn.CreateCommand();

                //Delete Record
                command.CommandText = "DELETE FROM Project WHERE Name = '" + projname + "'";
                command.ExecuteNonQuery();

                conn.Close();
                return true;
            }

            return false;
        }

        public static int GetRecordProperty(string projname, string property)
        {
            int retval = -1;
            SQLiteConnection conn = new SQLiteConnection("Data Source=TimeTracker.db");

            conn.Open();

            var command = conn.CreateCommand();

            //Count number of records in table
            command.CommandText = "SELECT " + property + " FROM Project WHERE Name = '" + projname + "'";
            retval = Convert.ToInt32(command.ExecuteScalar());

            conn.Close();

            return retval;
        }
    }
}
