﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Globalization;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Controls;
using System.Windows.Data;
using TimeTracker.Objects;
using TimeTracker.ViewModels;

namespace TimeTracker
{
    class HoursHelper
    {
        public static double SumTotalHoursEntered(ObservableCollection<object> projects)
        {
            double retval = 0;

            foreach (ProjectRecordViewModel pr in projects)
            {
                double hours = HoursEntered(pr);
                if (hours > 0)
                {
                    retval += hours;
                }
            }

            return retval;
        }

        public static double HoursEntered(ProjectRecordViewModel pr)
        {
            double totalHours = 0;

            totalHours += pr.Sun;
            totalHours += pr.Mon;
            totalHours += pr.Tue;
            totalHours += pr.Wed;
            totalHours += pr.Thu;
            totalHours += pr.Fri;
            totalHours += pr.Sat;

            return totalHours;
        }
    }
}
