﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Text.RegularExpressions;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using TimeTracker.ViewModels;

namespace TimeTracker.Objects
{
    /// <summary>
    /// Interaction logic for ProjectRecord.xaml
    /// </summary>
    public partial class ProjectRecord : UserControl
    {
        public ProjectRecord()
        {
            InitializeComponent();
        }

        // TODO: If you use a double data type for each day, this is automatically taken care of
        private void Hours_OnPreviewTextInput(object sender, TextCompositionEventArgs e)
        {
            Regex regex = new Regex(@"^(10|\d)(\.\d{1,2})?$");
            e.Handled = !regex.IsMatch(e.Text);
        }
    }
}
