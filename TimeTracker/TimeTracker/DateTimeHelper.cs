﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace TimeTracker
{
    public static class DateTimeHelper
    {
        public static DateTime StartOfWeek => DateTime.Today.AddDays(-(int)DateTime.Today.DayOfWeek);

        public static DateTime FirstDayOfWeek(DateTime dt)
        {
            return dt.AddDays(-(int)dt.DayOfWeek);
        }
    }
}
