﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Input;
using McsPro.Common.Patterns.Command;
using McsPro.Common.Patterns.Observer;
using TimeTracker.Objects;

namespace TimeTracker.ViewModels
{
    public class ProjectRecordViewModel : PropertyChangedBase
    {
        public ProjectRecordViewModel(string name)
        {
            ProjectName = name;
            if (name == "Admin")
                Opacity = 0.5;
            else
                Opacity = 1;
        }

        private bool isEnabled_;
        public bool IsEnabled
        {
            get { return isEnabled_; }
            set
            {
                isEnabled_ = value;
                OnPropertyChanged();
            }
        }

        private double opacity_;
        public double Opacity
        {
            get { return opacity_; }
            set
            {
                opacity_ = value;
                OnPropertyChanged();
            }
        }
        
        private string projectName_;
        public string ProjectName
        {
            get { return projectName_; }
            set
            {
                projectName_ = value;
                OnPropertyChanged();
            }
        }

        private double sun_;

        public double Sun
        {
            get { return sun_; }
            set
            {
                value = sun_;
                OnPropertyChanged();
                MainWindowViewModel.This.OnPropertyChanged("SumHoursText");
            }
        }

        public double Mon { get; set; }
        public double Tue { get; set; }
        public double Wed { get; set; }
        public double Thu { get; set; }
        public double Fri { get; set; }
        public double Sat { get; set; }

        private RelayCommand deleteRowCommand_;
        public RelayCommand DeleteRowCommand => deleteRowCommand_ ?? (deleteRowCommand_ = new RelayCommand(DeleteRow, CanDeleteRow));

        private RelayCommand directionKeyCommand_;
        public RelayCommand DirectionKeyCommand => directionKeyCommand_ ?? (directionKeyCommand_ = new RelayCommand(DirectionKey));

        private bool CanDeleteRow(object parameter)
        {
            return !(Opacity < 1);
        }

        private void DeleteRow(object parameter)
        {
            MainWindowViewModel.This.ShownProjectCollection.Remove(this);
            var t =  MainWindowViewModel.This.CompleteProjectCollection;
            t.Insert(t.Count - 1, ProjectName);
        }

        private static void DirectionKey(object parameter)
        {
            var u = Keyboard.FocusedElement as UIElement;

            switch (parameter.ToString())
            {
                case "Left":
                    u.MoveFocus(new TraversalRequest(FocusNavigationDirection.Left));
                    break;
                case "Right":
                    u.MoveFocus(new TraversalRequest(FocusNavigationDirection.Right));
                    break;
                case "Up":
                    u.MoveFocus(new TraversalRequest(FocusNavigationDirection.Up));
                    break;
                case "Down":
                    u.MoveFocus(new TraversalRequest(FocusNavigationDirection.Down));
                    break;
            }
        }
    }
}
