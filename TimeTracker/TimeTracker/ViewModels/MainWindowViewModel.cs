﻿using System;
using System.Collections.ObjectModel;
using System.Diagnostics;
using System.Reflection;
using System.Runtime.Remoting.Channels;
using McsPro.Common.Patterns.Command;
using McsPro.Common.Patterns.Observer;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Input;
using TimeTracker.Objects;

namespace TimeTracker.ViewModels
{
    public class MainWindowViewModel : PropertyChangedBase
    {
        public static MainWindowViewModel This { get; private set; }

        private string title_;

        public string Title
        {
            get { return title_; }
            set
            {
                title_ = value;
                OnPropertyChanged();
            }
        }

        public string UserName { get; set; }

        private string selectedProjectName_;

        public string SelectedProjectName
        {
            get { return selectedProjectName_; }
            set
            {
                selectedProjectName_ = value;
                OnPropertyChanged();
            }
        }
        
        public string SumHoursText => "Sum Hours: " + HoursHelper.SumTotalHoursEntered(ShownProjectCollection);

        private int selectedProjectIndex_;
        public int SelectedProjectIndex
        {
            get { return selectedProjectIndex_; }
            set
            {
                selectedProjectIndex_ = value;
                OnPropertyChanged();
            }
        }

        private DateTime beginDateTime_;
        public DateTime BeginDateTime
        {
            get { return beginDateTime_; }
            set
            {
                beginDateTime_ = DateTimeHelper.FirstDayOfWeek(value);
                OnPropertyChanged();
            }
        }

        private ObservableCollection<object> completeProjectCollection_;
        public ObservableCollection<object> CompleteProjectCollection
        {
            get { return completeProjectCollection_; }
            set
            {
                completeProjectCollection_ = value;
                OnPropertyChanged();
            }
        }

        private ObservableCollection<object> shownProjectCollection_;
        public ObservableCollection<object> ShownProjectCollection
        {
            get { return shownProjectCollection_; }
            set
            {
                shownProjectCollection_ = value;
                OnPropertyChanged();
            }
        }
        
        public readonly ObservableCollection<object> DefaultListCollection = new ObservableCollection<object>()
        {
            "",
            "Personal Leave",
            "Public Holiday",
            "Vacation"
        };

        private RelayCommand submitCommand_;
        public RelayCommand SubmitCommand => submitCommand_ ?? (submitCommand_ = new RelayCommand(Submit, CanSubmit));

        private RelayCommand deleteCommand_;
        public RelayCommand DeleteCommand => deleteCommand_ ?? (deleteCommand_ = new RelayCommand(Delete, CanDelete));

        private RelayCommand addProjectCommand_;
        public RelayCommand AddProjectCommand => addProjectCommand_ ?? (addProjectCommand_ = new RelayCommand(AddProject, CanAddProject));
        

        public MainWindowViewModel()
        {
            // Setup MainWindow Title with version info
            var assembly = Assembly.GetExecutingAssembly();
            var fileVersionInfo = FileVersionInfo.GetVersionInfo(assembly.Location);
            string version = fileVersionInfo.ProductVersion;

            Title = $"Timesheet Tracker version {version}";

            This = this;
            BeginDateTime = DateTime.Today;

            CompleteProjectCollection = new ObservableCollection<object>();
            DatabaseHelper.SetupDB();
            
            //Create disabled combo box item for createproject control
            CreateProject cp = new CreateProject { IsTabStop = false };
            CompleteProjectCollection.Add(cp);
            
            ShownProjectCollection = new ObservableCollection<object>();
            var pr = new ProjectRecordViewModel("Admin");
            ShownProjectCollection.Add(pr);
        }


        private bool CanDelete(object param)
        {
            // Don't allow delete if it is the first or last item in the combobox
            int canIDelete = DatabaseHelper.GetRecordProperty(SelectedProjectName, "CanDelete");
            return canIDelete == 1;
        }

        private void Delete(object param)
        {
            bool retval = DatabaseHelper.DeleteRecord(SelectedProjectName);
            if (retval)
            {
                CompleteProjectCollection.Remove(SelectedProjectName);
                SelectedProjectIndex = 0;
            }
        }

        private bool CanSubmit(object parameter)
        {
            // Check if username has been entered
            if (string.IsNullOrEmpty(UserName))
            {
                return false;
            }

            // Check if there are any hours entered on form
            if (HoursHelper.SumTotalHoursEntered(ShownProjectCollection) <= 0)
            {
                return false;
            }   

            return true;
        }
        
        private void Submit(object parameter)
        {
            if (MessageBox.Show("Are you ready to submit the data for processing?", "Are you ready?",
                MessageBoxButton.OKCancel) == MessageBoxResult.OK)
            {
                Cursor defaultCursor = Mouse.OverrideCursor;

                Mouse.OverrideCursor = Cursors.Wait;
                bool success = GenerateDocuments.BuildExcelFiles(UserName, BeginDateTime, ShownProjectCollection, DefaultListCollection);

                Mouse.OverrideCursor = defaultCursor;
                // Close Main program after successful build
                if (success)
                    Application.Current.MainWindow.Close();
                else
                    MessageBox.Show("There was an issue generating your documents.");
            }
        }
        
        private bool CanAddProject(object parameter)
        {
            // If name is null or empty, false
            if (string.IsNullOrEmpty(SelectedProjectName))
            {
                return false;
            }

            // Look through all Shown projects, if the selected name already exists in the shown list, false;
            // TODO: Can be made into a simple linq statement (something like return !ShownProjectCollection.Any(x => x.ProjectName == SelectedProjectName))
            foreach (ProjectRecordViewModel prvm in ShownProjectCollection)
            {
                if (prvm.ProjectName == SelectedProjectName)
                {
                    return false;
                }
            }

            // Look through combo box items, if the item selected is not of type string, false;
            foreach (object obj in CompleteProjectCollection)
            {

                if (obj.ToString() == SelectedProjectName && obj.GetType() != typeof(string))
                {
                    return false;
                }
            }

            return true;
        }

        private void AddProject(object parameter)
        {
            var pr = new ProjectRecordViewModel(SelectedProjectName);
            ShownProjectCollection.Add(pr);
            CompleteProjectCollection.Remove(pr.ProjectName);
            SelectedProjectIndex = 0;
        }
    }
}
