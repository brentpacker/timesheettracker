﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.CompilerServices;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Input;
using McsPro.Common.Extensions;
using McsPro.Common.Patterns.Command;
using McsPro.Common.Patterns.Observer;

namespace TimeTracker.ViewModels
{
    class CreateProjectViewModel : PropertyChangedBase
    {
        private string project_;
        public string Project
        {
            get { return project_; }
            set
            {
                project_ = value;
                OnPropertyChanged();
            }
        }

        private RelayCommand createCommand_;
        public RelayCommand CreateCommand => createCommand_ ?? (createCommand_ = new RelayCommand(Create, CanCreate));

        private bool CanCreate(object parameter)
        {
            return !string.IsNullOrEmpty(Project) && Project.Trim() != string.Empty && DatabaseHelper.GetRecordProperty(Project.Trim().ToTitleCase(), "count(*)") <= 0;
        }

        private void Create(object parameter)
        {
            DatabaseHelper.InsertRecord(Project.Trim().ToTitleCase());
            Project = "";
        }
    }
}
